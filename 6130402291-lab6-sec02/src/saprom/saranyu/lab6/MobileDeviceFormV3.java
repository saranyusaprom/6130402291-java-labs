package saprom.saranyu.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	protected JLabel FeaturesLabel;
	protected JTextArea FeaturesArea;
	protected JList Featureslist;
	protected JMenuBar MenuBar;
	protected JMenu FileMenu;
	protected JMenu ConfigMenu;
	protected JMenuItem NewItem;
	protected JMenuItem OpenItem;
	protected JMenuItem SaveItem;
	protected JMenuItem ExitItem;
	protected JMenu ColorItem;
	protected JMenu SizeItem;
	protected JPanel CenPanel;


	public MobileDeviceFormV3(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();
		CenPanel = new JPanel();
		FeaturesLabel=new JLabel("Features:");
		
		Featureslist=new JList();
		String list[]= { "Design and build quality","Great Camera","Screen", "Battery Life"}; 
		Featureslist= new JList(list);
	
		CenPanel.setLayout(new BorderLayout());

		CenPanel.add(FeaturesLabel,BorderLayout.WEST);
		CenPanel.add(Featureslist,BorderLayout.EAST);
		NorthPanel2.add(CenPanel);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


	}
	protected void addMenus() {
		MenuBar = new JMenuBar();
		
		FileMenu = new JMenu("File");
		ConfigMenu = new JMenu("Config");
		MenuBar.add(FileMenu);
		MenuBar.add(ConfigMenu);
		NewItem = new JMenuItem("New");
		OpenItem = new JMenuItem("Open");
		SaveItem = new JMenuItem("Save");
		ExitItem = new JMenuItem("Exit");
		ColorItem = new JMenu("Color");
		SizeItem = new JMenu("Size");
		

		FileMenu.add(NewItem);
		FileMenu.add(OpenItem);
		FileMenu.add(SaveItem);
		FileMenu.add(ExitItem);
		ConfigMenu.add(ColorItem);
		ConfigMenu.add(SizeItem);

		this.setJMenuBar(MenuBar);

	}
	public static void createAndShowGUI() {
		 MobileDeviceFormV3 MobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");
		 MobileDeviceFormV3.addComponents();
		 MobileDeviceFormV3.addMenus();
		 MobileDeviceFormV3.setFrameFeatures();
	 }										
													
	 public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	 }
}
