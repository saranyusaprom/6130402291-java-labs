package saprom.saranyu.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	
	protected JLabel TypeLabel;
	protected JPanel TypePanel;
	protected JPanel ReviewPanel;
	protected JPanel NorthPanel2;
	protected JLabel ReviewLabel;
	protected JComboBox Box;
	protected JTextArea ReviewArea;
	protected JScrollPane Scroll;
	protected JPanel ButtomPanel;

	
	public MobileDeviceFormV2(String title) {
		super(title);
	}
	protected void addComponents() {
		super.addComponents();
		NorthPanel2=new JPanel();
		ButtomPanel=new JPanel();
		ReviewPanel = new JPanel();
		TypePanel=new JPanel();
		ReviewArea=new JTextArea(2,35);
		ReviewLabel=new JLabel("Review:");
		
		
		TypeLabel = new JLabel("Type:");
		String[] show= {"Phone","Tablet","Smart TV"};
		Box=new JComboBox(show);
		
		ReviewLabel = new JLabel("Review:");
		ReviewArea.setText("Bigger than previous Note phones in every way, the Samsung Galaxy Note 9 has a larger 6.4-inch screen, heftier 4,000mAh battery, and a massive 1TB of storage option.");
		ReviewArea.setLineWrap(true);
		ReviewArea.setWrapStyleWord(true);
		Scroll=new JScrollPane(ReviewArea);
		Scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		

		TypePanel = new JPanel();
		TypePanel.setLayout(new BorderLayout());
		TypePanel.add(TypeLabel,BorderLayout.WEST);
		TypePanel.add(Box,BorderLayout.EAST);

		ReviewPanel = new JPanel();
		ReviewPanel.setLayout(new BorderLayout());
		ReviewPanel.add(ReviewLabel,BorderLayout.NORTH);
		ReviewPanel.add(Scroll,BorderLayout.SOUTH);
		

		NorthPanel2 = new JPanel();
		NorthPanel2.setLayout(new BorderLayout());
		NorthPanel2.add(TypePanel,BorderLayout.NORTH);
		NorthPanel2.add(ReviewPanel,BorderLayout.SOUTH);
		NorthPanel1.add(NorthPanel2,BorderLayout.SOUTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public static void createAndShowGUI() {
		 MobileDeviceFormV2 MobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");
		 MobileDeviceFormV2.addComponents();
		 MobileDeviceFormV2.setFrameFeatures();
	 }										
													
	 public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	 }
}
