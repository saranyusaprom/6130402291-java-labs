/**
* This Java program do not want the argument,but it will show these.
* 
* "My name is <Sting of my name>"
* "My student ID is <String of my id>"
* "<My first alphabet of my name> <boolean> <my last two digits number of my id from base8 to base10> <my last two digits number of my id from base16 to base10>"
* "<last two digits number on data type long> <last two digits number on data type float> <last two digits number on data type double>"
* 
*Author: Saranyu Saprom
*ID: 613040229-1
*Sec:2
*Date: January 21, 2019
**/
package saprom.saranyu.lab2;

public class Datatype {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String n="Saranyu Saprom";
		String i="6130402291";
		char first=n.charAt(0);
		boolean check = true;
		int myIDInOctal=0133;
		int hexadecimal=0x5B;
		long twodigit=91;
		float twodigitf =34.61F;
		double twodigitd =34.61D;
		
		System.out.println("My name is "+n);
		System.out.println("My student ID is "+i);
		System.out.println(first+" "+check+" "+myIDInOctal+" "+hexadecimal);
		System.out.println(twodigit+" "+twodigitf+" "+twodigitd);
		

	}

}
