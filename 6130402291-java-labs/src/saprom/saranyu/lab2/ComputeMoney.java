/**
* This Java program accept four arguments:first is number of bank 1000Bath,second is number of bank 500Bath,third is number of bank 100 Bath and forth is number of bank 20 Bath
* Run:It will show you a summation of money of your four arguments
* Total Money is <your summation>
* 
* 
* Author: Saranyu Saprom
* ID: 613040229-1
* Sec: 2
* Date: January 21, 2019
**/
package saprom.saranyu.lab2;

public class ComputeMoney {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length != 4) {
			System.err.println("ComputeMoney <1,000 Bath> <500 Bath> <100 Bath> <20 Bath>");
		}
		double thousand=Double.parseDouble(args[0]);
		double fifhun=Double.parseDouble(args[1]);
		double hundred=Double.parseDouble(args[2]);
		double yisib=Double.parseDouble(args[3]);
		
		double sum;
		sum= (thousand*1000)+(fifhun*500)+(hundred*100)+(yisib*20);
		System.out.println("Total Money is "+sum);

	}

}
