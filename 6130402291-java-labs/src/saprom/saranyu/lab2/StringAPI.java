/**
* This Java program accept only one argument:The argument is your place of education 
* if argument has a word of university,It will show you a     "<your argument> is a university"
* else if argument has a word of technical,It will show you a      "<your argument> is a college"
* else it will show you a     "<your argument> is neither a university nor a college"
* 
* Author: Saranyu Saprom
* ID: 613040229-1
* Sec: 2
* Date: January 21, 2019
**/

package saprom.saranyu.lab2;

public class StringAPI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean university=args[0].toLowerCase().contains("university");
		boolean college=args[0].toLowerCase().contains("technical");
		if (university==true) {
			System.out.println(args[0]+" is a university");
		}else if (college ==true) {
			System.out.println(args[0]+" is a college ");
		}else {
			System.out.println(args[0]+" is neither a university nor a college");
		}
			
	}

}
