/**
* This Java program that accept five arguments :five arguments are numbers what you want to sort from min to max
* 
* 
* Author: Saranyu Saprom
* ID: 613040229-1
* Sec: 2
* Date: January 21, 2019
**/
package saprom.saranyu.lab2;
import java.util.Arrays; 

public class SortNumbers {
	public static void main(String[] args) {
		float one=Float.parseFloat(args[0]);
		float two=Float.parseFloat(args[1]);
		float three=Float.parseFloat(args[2]);
		float four=Float.parseFloat(args[3]);
		float five=Float.parseFloat(args[4]);
		float[] arraylist = {one, two, three, four, five};
		Arrays.sort(arraylist); 
		for (int i=0 ; i<arraylist.length;i++) {
			System.out.print(arraylist[i]+" " );
		}

	}

}
