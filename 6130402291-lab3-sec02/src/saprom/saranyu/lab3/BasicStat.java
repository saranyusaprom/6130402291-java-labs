
package saprom.saranyu.lab3;

import java.util.*;

/**
 * 
 * This java program get amount argument depend on args[0] 
 * For example:: 5 1 5 3 6 7 
 * 
 * And this program can calculate min,max,average and The standard deviation.
 * 
 * 
 * @author Saranyu Saprom
 * Sec:: 2
 * ID:: 613040229-1
 * Date:: 03/02/2019
 *
 */
public class BasicStat {
	
	static double num[];
	static int numInput; 
	
	/**
	 * This method will check your entered.
	 * @param args your input numbers
	 */
	public static void acceptInput(String[] args) {
		
		numInput = Integer.parseInt(args[0]);
		num = new double[args.length-1];
		String arglength = new Integer(args.length-1).toString();
		
		if(args[0].equals(arglength)) {
			for (int i = 0 ; i < numInput ; i++) {
				double arg=Double.parseDouble(args[i+1]);
				num[i]=arg;}
		}
		else {
			System.err.println("<BasicStat> <numNumbers> <numbers>...");
		}
	}
	
	/**
	 * This method is calculate min,max,average and The standard deviation.
	 */
	public static void displayStats() {
		
		Arrays.sort(num);
		double max = num[numInput-1];
		double min = num[0];
		double sum = 0;
		
		for (int count = 0 ; count <= numInput-1 ; count++) {
			sum += num[count];
		}
		
		double avg = sum/(numInput);
		double sigma = 0;

		for (int count = 0 ; count <= numInput-1 ; count++) {
			sigma += (num[count]-avg)*(num[count]-avg);
		}
		
		double standevia = Math.sqrt(sigma/numInput);
		
		System.out.println("Max is "+max+" Min is "+min+".");
		System.out.println("Average is "+avg);
		System.out.println("The standard deviation is "+standevia);
		
	}
	/**
	 * @param args your input numbers.
	 */
	public static void main(String[] args) {
		
		acceptInput(args);
		displayStats();
	}
}