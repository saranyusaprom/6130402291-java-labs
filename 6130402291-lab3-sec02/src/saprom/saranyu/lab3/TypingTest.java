
package saprom.saranyu.lab3;

import java.util.*;

/**
 * This java program can check how fast you can dictation.
 * 
 * Run::Console will show you 7 color by random and you will dictation like console.
 * if you finish before 12 seconds that mean you type faster than average person.
 * else you type slower than average person.
 * 
 * @author Saranyu Saprom 
 * Sec:: 2
 * ID:: 613040229-1
 * Date:: 03/02/2019
 *
 */
public class TypingTest {
	
    public static final double createdMillis = System.currentTimeMillis();
    
    /**
     * This method is show the Time of your speed.
     */
    public static void Timeans() {
    	
        double nowMillis = System.currentTimeMillis();
        double anstime = (nowMillis - createdMillis) / 1000;
        System.out.println("Your Time is "+anstime+".");
        if (anstime <= 12) {
			System.out.println("You type faster than average person");
		}
		else {
			System.out.println("You type slower than average person");	
		}
		System.exit(0);
    }
    
    /**
     * This method is print from random 7 colors.
     * @param rainbowlist list of 7 colors.
     * @return random 7 colors on console.
     */
    public static String random(String[] rainbowlist) {
    	
    	String output = "";
    	
		for (int i = 0; i < 8 ; i++) {
			int randnum = (int) (Math.random() * 7);
			
			if (i == 7) {
				output += rainbowlist[randnum];
			}
			else {
				output += rainbowlist[randnum] + " ";
			}
		}	
		return output;
	}
    
    
	/**
	 * This method get input from you.
	 * @param args 7 colors you will enter correctly.
	 */
	public static void main(String[] args) {
		
		String[] rainbowlist = {"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"};
		String write = random(rainbowlist);
		System.out.println(write);
		
		while(true) {
			System.out.print("Type your answer: ");	
			Scanner sc=new Scanner(System.in);
			String peoplewrite = sc.nextLine().toLowerCase();
			
			if (peoplewrite.equalsIgnoreCase(write)) {
				Timeans();
				sc.close();
				break;
			}
			else {
				continue;
			}	
		}
	}	
}
