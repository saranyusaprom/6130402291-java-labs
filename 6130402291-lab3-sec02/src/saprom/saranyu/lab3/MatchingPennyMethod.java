
package saprom.saranyu.lab3;

import java.util.Scanner;

/**
 * 
 * This java program is like a game this program want 1 argument (tail or head only!!!).
 *  If the choices are the same, the user wins. 
 *  If the choices are different, the computer wins.
 *  
 * @author Saranyu Saprom 
 * Sec:: 2
 * ID:: 613040229-1
 *Date::03/02/2019
 */
public class MatchingPennyMethod {
	
	/**
	 * This method computer will guess.
	 * @return computer guess tail or head
	 */
	public static String genComChoice() {
		
		int number = (int)(Math.random()*2);
		if (number==0) {
			return "tail";
		}else {
			return "head";
		}
	}
	/**
	 * This method will save what word you entered.
	 * @param get your guess word
	 * @return your guess word
	 */
	public static String acceptInput(String get) { 
		
		if (get.equals("head")|| get.equals("tail") || get.equals("exit")) {
			return get;
		}else {
			System.err.println("Incorrect input. head or tail only");
			return get="";
		}		
	}
	
	
	/**
	 * This method check who is the winner.
	 * @param get your guess word (tail or head)
	 * @param comran computer's word guess (tail or head)
	 */
	public static void displayWiner(String get , String comran) {
		
		if (get.equals(comran)) {
			System.out.println("You play "+get);
			System.out.println("Computer play "+comran);
			System.out.println("You wins.");
		}else if (get != comran ) {
			System.out.println("You play "+get);
			System.out.println("Computer play "+comran);
			System.out.println("Computers wins.");}
	}
	
	/**
	 * This is main method,main method will get input if input is exit program will end.
	 * @param args your word entered (tail or head).
	 */
	public static void main(String[] args) {
		
		while (true) {
			String comran;
			comran=genComChoice();
			
			System.out.print("Enter head or tail : ");
			Scanner sc=new Scanner(System.in);
			String get;
			get = sc.nextLine().toLowerCase();
			acceptInput(get);
			
			if (get.equals("head")|| get.equals("tail")) {
				displayWiner(get , comran);
			}else if (get.equals("exit")){
				System.out.println("Good Bye.");
				System.exit(0);
			}
		}
	}
	

}