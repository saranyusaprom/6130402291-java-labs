package saprom.saranyu.lab8;

import java.awt.geom.Ellipse2D;

public class MyBall extends Ellipse2D.Double {
	 protected final static int diameter=30;
	 
	 public MyBall(int x,int y){
		 super(x , y ,diameter, diameter);
	 }
}
