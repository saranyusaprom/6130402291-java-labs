package saprom.saranyu.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV2 extends MyFrame {

	public MyFrameV2(String text) {
		super(text);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	protected void addComponents() {
		add(new MyCanvasV2());
	}
}
