package saprom.saranyu.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV2 extends MyCanvas {
	MyBall ball = new MyBall((WIDTH/2)-15,(HEIGHT/2)-15); 
	MyPedal pedal = new MyPedal((WIDTH/2)-50,(HEIGHT)-10);
	MyBrick brick = new MyBrick(360,0);
	
	public MyCanvasV2() {
		super();
	}
	@Override
	public void paint(Graphics g) {
		super.paintComponent(g); 
		Graphics2D g2d = (Graphics2D)g; 
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, 800, 600);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		g2d.fill(brick);
		g2d.fill(pedal);
		g2d.drawLine(400, 0, 400, 600);
		g2d.drawLine(0, 300, 800, 300);



		
	}
	
}
