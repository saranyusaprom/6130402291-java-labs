package saprom.saranyu.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.List;

public class MyCanvasV3 extends MyCanvasV2{
	MyBall ballonleft = new MyBall((WIDTH/2)-400,(HEIGHT/2)-300); 
	MyBall ballonright = new MyBall((WIDTH)-30,(HEIGHT/2)-300); 
	MyBall ballunderleft = new MyBall((WIDTH/2)-400,(HEIGHT)-30); 
	MyBall ballunderright = new MyBall((WIDTH)-30,(HEIGHT)-30); 
	MyBrick brick = new MyBrick(0,(HEIGHT / 2) - 10);
	
	public MyCanvasV3() {
		super();
	}
	@Override
	public void paint(Graphics g) {
		super.paintComponent(g); 
		
		Graphics2D g2d = (Graphics2D)g; 
		g2d.setPaint(Color.BLACK);
		g2d.fillRect(0, 0, 800, 600);
		g2d.setPaint(Color.WHITE);
		g2d.fill(ballonleft);
		g2d.fill(ballonright);
		g2d.fill(ballunderleft);
		g2d.fill(ballunderright);

		
			
		int width=80;
		for(int count=0;count<=9;count++) {
			g2d.setStroke(new BasicStroke(5));
			brick.x=0+count*width;
			brick.y=290;
			g2d.setColor(Color.WHITE);
			g2d.fill(brick);
			g2d.setColor(Color.BLACK);
			g2d.draw(brick);

	}

		
		
	}
}
