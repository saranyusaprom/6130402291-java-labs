package saprom.saranyu.lab4;

public class Automobile {
	private int gasoline;
	private int speed;
	private int maxSpeed;
	private int acceleration;
	private String model;
	private Color color;
	private static int numberOfAutomobile = 0;

	public enum Color {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK
	}

	public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile++;
	}

	public Automobile() {
		this.gasoline = 0;
		this.speed = 0;
		this.acceleration = 0;
		this.maxSpeed = 160;
		this.model = "Automobile";
		this.color = Color.WHITE;
		numberOfAutomobile++;
	}

	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}

	public int getGasoline() {
		return gasoline;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getSpeed() {
		return speed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	public int getAcceleration() {
		return acceleration;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getModel() {
		return model;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}
	
	@Override
	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}

	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}
}
