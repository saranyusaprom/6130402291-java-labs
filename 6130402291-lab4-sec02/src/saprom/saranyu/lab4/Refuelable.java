package saprom.saranyu.lab4;

/**
 * @author Saranyu Saprom
 * 
 * 
 * 
 * This is fuel of car.
 *
 */
public interface Refuelable {
	
	public void refuel();
}	

