package saprom.saranyu.lab4;

import saprom.saranyu.lab4.Automobile.Color;

public class TestDrive {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Automobile car1 = new Automobile(100, 0, 200, 0, "Sport car", Color.RED);
		Automobile car2 = new Automobile(100, 0, 100, 0, "Electric car", Color.WHITE);
		System.out.println("Car 1's max speed is " + car1.getMaxSpeed());
		System.out.println("Car 2's max speed is " + car2.getMaxSpeed());
		car1.setMaxSpeed(280);
		System.out.println("Car 1;s max speed has increase to " + car1.getMaxSpeed());
		System.out.println("There are " + Automobile.getNumberOfAutomobile() + " automobile");
		Automobile car3 = new Automobile();
		System.out.println("Now there are " + Automobile.getNumberOfAutomobile() + " automobile");
		System.out.println(car1);
		System.out.println(car3);
		ToyotaAuto car5 = new ToyotaAuto(200, 50, "Burotang");
		System.out.println(car5);
		car5.accelerate();
		car5.accelerate();
		car5.accelerate();
		car5.brake();
		System.out.println(car5.getSpeed());
		System.out.println(car5);

	}
}
