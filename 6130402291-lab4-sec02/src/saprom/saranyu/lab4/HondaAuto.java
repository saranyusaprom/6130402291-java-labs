package saprom.saranyu.lab4;

/**
 * @author Saranyu Saprom
 * 
 * This java program is subclass of Automobile,These method is a movement of car.
 *
 */
public class HondaAuto extends Automobile implements Moveable ,Refuelable{

	/**
	 * This constructor is keep input from user.
	 * @param maxspeed  is a max speed of car,then send to keep in class Automobile.
	 * @param acceleration is a acceleration of car,then send to keep in class Automobile.
	 * @param model is a model of car,then send to keep in class Automobile.
	 */
	public HondaAuto(int maxspeed,int acceleration,String model) {
		setGasoline(100);
		setMaxSpeed(maxspeed);
		setModel(model);
		setAcceleration(acceleration);
	}
	@Override
	public void refuel() {
		setGasoline(100);
		System.out.println(getModel()+" refuels");
	}
	@Override
	public void accelerate() {
		setSpeed(getSpeed() + getAcceleration());
		if (getSpeed() > getMaxSpeed()) {
			setSpeed(getMaxSpeed());
		}
		setGasoline(getGasoline() - 10);
		System.out.println(getModel() + " accelerates");
	}
	@Override
	public void brake() {
		setSpeed(getSpeed() - getAcceleration());
		if (getSpeed() < 0) {
			setSpeed(0);
		}
		setGasoline(getGasoline() - 10);
		System.out.println(getModel() + " brakes");
	}
	@Override
	public void setSpeed(int speed) {
		super.setSpeed(speed);
		if (getSpeed() < 0) {
			super.setSpeed(0);
		} else if (getSpeed() > getMaxSpeed()) {
			super.setSpeed(getMaxSpeed());
		}
	}
	
	@Override
	public String toString() {
		return getModel() + " gas:" + getGasoline() + " speed:" + getSpeed() + " maxSpeed:" + getMaxSpeed()
				+ " acceleration:" + getAcceleration();
	}
	
}
