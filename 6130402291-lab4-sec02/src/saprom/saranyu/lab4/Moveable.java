package saprom.saranyu.lab4;

/**
 * @author Saranyu Saprom
 * 
 *
 * 
 * This is Movement of car.
 */
public interface Moveable {
		public void accelerate();
		public void brake();
		public void setSpeed(int speed);
		
}
