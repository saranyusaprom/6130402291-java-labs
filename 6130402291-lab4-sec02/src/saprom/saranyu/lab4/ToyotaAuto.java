package saprom.saranyu.lab4;

/**
 * @author Saranyu Saprom
 * 
 * 
 * 
 * This java program is subclass of Automobile,These method is a movement of car.
 *
 */
public class ToyotaAuto extends Automobile implements Moveable, Refuelable {

	/**
	 * This constructor is keep input from user.
	 * @param maxSpeed is a max speed of car,then send to keep in class Automobile.
	 * @param acceleration is a acceleration of car,then send to keep in class Automobile.
	 * @param model is a model of car,then send to keep in class Automobile.
	 */
	public ToyotaAuto(int maxSpeed, int acceleration, String model) {
		setMaxSpeed(maxSpeed);
		setAcceleration(acceleration);
		setModel(model);
		setGasoline(100);
	}

	public void refuel() {
		setGasoline(100);
		System.out.println(getModel() + " refuels");
	}

	public void accelerate() {
		setSpeed(getSpeed() + getAcceleration());
		if (getSpeed() > getMaxSpeed()) {
			setSpeed(getMaxSpeed());
		}
		setGasoline(getGasoline() - 15);
		System.out.println(getModel() + " accelerates");
	}

	public void brake() {
		setSpeed(getSpeed() - getAcceleration());
		if (getSpeed() < 0) {
			setSpeed(0);
		}
		setGasoline(getGasoline() - 15);
		System.out.println(getModel() + " brakes");
	}

	public void setSpeed(int speed) {
		super.setSpeed(speed);
		if (getSpeed() < 0) {
			super.setSpeed(0);
		} else if (getSpeed() > getMaxSpeed()) {
			super.setSpeed(getMaxSpeed());
		}
	}
	
	@Override
	public String toString() {
		return getModel() + "gas:" + getGasoline() + " speed:" + getSpeed() + " max speed:" + getMaxSpeed() + " acceleration:"
				+ getAcceleration();
	}

}
