package saprom.saranyu.lab4;

/**
 * @author Saranyu Saprom
 *
 *This java program is main class and get input from user.
 */
public class TestDrive2 {
	public static void isFaster(Automobile car1, Automobile car2) {
		
		if (car1.getSpeed() < car2.getSpeed()) {
			System.out.println(car1.getModel() + " is NOT faster than " + car2.getModel());
		} else {
			System.out.println(car1.getModel() + " is faster than " + car2.getModel());
		}
	}

	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");

		System.out.println(car1);
		System.out.println(car2);

		car1.accelerate();
		car2.accelerate();
		car2.accelerate();

		System.out.println(car1);
		System.out.println(car2);

		car1.brake();
		car1.brake();
		car2.brake();

		System.out.println(car1);
		System.out.println(car2);

		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
		isFaster(car2, car1);
	}
}
