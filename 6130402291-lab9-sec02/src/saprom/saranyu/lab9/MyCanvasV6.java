package saprom.saranyu.lab9;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	protected Thread running = new Thread(this);

	public MyCanvasV6() {
		ball = new MyBallV2((MyCanvas.WIDTH / 2) - (MyBall.diameter / 2), (MyCanvas.HEIGHT / 2) - (MyBall.diameter / 2));
		ball.ballVelX = 1;
		ball.ballVelY = 1;
		running.start();
	}
	
	@Override
	public void run() {
		while(true) {
			//
			if (ball.x + ball.diameter == MyCanvas.WIDTH) {
				ball.ballVelX *= -1;
			} if (ball.x  == 0) { 
				ball.ballVelX *= -1;
			}
			//
			if (ball.y + ball.diameter == MyCanvas.HEIGHT) {
				ball.ballVelY *= -1;
			} if (ball.y  == 0) {
				ball.ballVelY *= -1;
			}
			//
			ball.move();
			repaint();
			//
	
			try {
				Thread.sleep(25);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}

}
