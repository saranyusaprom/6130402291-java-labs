package saprom.saranyu.lab9;

public class MyPedalV2 extends MyPedal{
	protected final static int speedPedal=20;
	public MyPedalV2(int x, int y) {
		super(x, y);
	}
	
	public void moveLeft() {
		if( x - speedPedal < 0) {
			x=0;
		}else {
			x-=speedPedal;
		}
	}
	public void moveRight() {
		if(speedPedal + x >MyCanvas.WIDTH) {
			x=MyCanvas.WIDTH - MyPedal.pedalWidth;
		}else {
			x+=speedPedal;
		}
	}

}
