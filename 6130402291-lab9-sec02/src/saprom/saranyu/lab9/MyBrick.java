package saprom.saranyu.lab9;


import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double {
	protected final static int brickWidth=80 ;
	protected final static int brickHeight=20;
	
	public MyBrick(int x,int y ) {
		super(x,y,brickWidth,brickHeight);
	}
}