package saprom.saranyu.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable{
	protected MyBallV2 ball = new MyBallV2(0, MyCanvas.HEIGHT/2 - MyBall.diameter/2);
	protected Thread running = new Thread(this);

	@Override
	public void run() {
		while(true) {
			if (ball.x + ball.diameter == WIDTH) {
				break;
			}
			ball.move();
			
			repaint();
	
			try {
				Thread.sleep(10);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}

	public MyCanvasV5() {
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g); 
		Graphics2D g2d = (Graphics2D)g; 
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0,0,super.WIDTH,super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		
	}
	
}
