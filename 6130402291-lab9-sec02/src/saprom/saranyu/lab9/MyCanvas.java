package saprom.saranyu.lab9;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	protected final static int WIDTH =800;
	protected final static int HEIGHT=600;

	public MyCanvas() {
		super();
		setPreferredSize();
		this.setBackground(Color.BLACK);
	}
	public void setPreferredSize() {
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g); 
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.WHITE);
		g2d.drawOval((WIDTH/2)-150,(HEIGHT/2)-150,300,300);
		g2d.fillOval((WIDTH/2)-55,(HEIGHT/2)-75,30,60);
		g2d.fillOval((WIDTH/2)+20,(HEIGHT/2)-75,30,60);
		g2d.fillRect((WIDTH/2)-50, (HEIGHT/2)+50, 100, 10);
		}
	
}