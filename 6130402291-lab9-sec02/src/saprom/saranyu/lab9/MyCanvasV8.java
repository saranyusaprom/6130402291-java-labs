package saprom.saranyu.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable ,KeyListener{
	protected int numCol = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks = new MyBrickV2[MyBrick.brickWidth][(super.HEIGHT / 3) - ( MyBrick.brickHeight)];
	protected Thread running = new Thread(this);
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,Color.RED };
	protected MyPedalV2 pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWidth / 2,MyCanvas.HEIGHT - MyPedal.pedalHeight);
	protected int lives=3;
	
	public MyCanvasV8() {
		ball = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2,MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball.ballVelX=0;
		ball.ballVelY=0;
		
		for(int i =0;i<numRow;i++) {
			for(int j=0;j<numCol;j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, (super.HEIGHT / 3) - (i * MyBrick.brickHeight)); 
			}
		}
		
		running.start();
		setFocusable(true);
		addKeyListener(this);
		
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setPaint(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setColor(Color.BLACK);
					g2d.setStroke(new BasicStroke(5));
					g2d.draw(bricks[i][j]);
				}
			}
		}
		//draw
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);

		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = " Lives : " + lives;
		g2d.drawString(s, 10, 30);
		
		if(numVisibleBricks ==0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 70));
			g2d.setColor(Color.GREEN);
			String win = "You won";
			g2d.drawString(win, MyCanvas.WIDTH / 3, MyCanvas.HEIGHT / 2);
			
		}
		if(lives==0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 72));
			g2d.setColor(Color.GRAY);
			String over = "GAME OVER";
			g2d.drawString(over, MyCanvas.WIDTH / 4, MyCanvas.HEIGHT / 2);
		}
	}
	private void checkPassBottom() {
		if (ball.y + MyBall.diameter >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.pedalWidth / 2 - MyBall.diameter / 2;
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}
	
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) { 
				ball.ballVelY *= -1;

			} else { 
				ball.ballVelX *= -1;

			}
			numVisibleBricks--;
			ball.move();
			brick.visible = false;
		}
	}

	private void collideWithPedal(MyBallV2 ball, MyPedalV2 pedal) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedal.pedalWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedal.pedalHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;

			} else {
				ball.ballVelX *= -1;

			}
			ball.move();
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) { 
			pedal.moveLeft();

		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			pedal.moveRight();

		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) { 
			ball.ballVelY = 4;
			Random random = new Random();
			int number = random.nextInt(2);
			if(number == 0) {
				 ball.ballVelX = -4;
			} if(number == 1) {
				 ball.ballVelX = 4;
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
	public void run() {
		while(true) {
			if (ball.x + MyBall.diameter >= MyCanvas.WIDTH) {
				ball.x = MyCanvas.WIDTH - MyBall.diameter;
				ball.ballVelX *= -1;
			} else if (ball.x + MyBall.diameter <= 30) {
				ball.x = 0;
				ball.ballVelX *= -1;
			}

			// bounce the ball when hit the top or the bottom of the wall
			else if (ball.y + MyBall.diameter >= MyCanvas.HEIGHT) {
				ball.y = MyCanvas.HEIGHT - MyBall.diameter;
				ball.ballVelY *= -1;
			} else if (ball.y + MyBall.diameter <= 30) {
				ball.y = 0;
				ball.ballVelY *= -1;
			}

			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision(ball, bricks[i][j]);
					}
				}
			}
			collideWithPedal(ball, pedal);
			checkPassBottom();

			
			
			ball.move();
			
			repaint();
			
			try {
				Thread.sleep(20);
			}catch(InterruptedException ex) {
				
			}
		}
	}
}
