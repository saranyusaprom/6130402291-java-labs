package saprom.saranyu.lab9;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame {
	
	public MyFrame(String string) {
		super(string);
	}
	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("MyFrame");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	protected void addComponents() {
		add(new MyCanvas());
	}
	public void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				createAndShowGUI();
			}
		});
	}
}