package saprom.saranyu.lab9;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV4 extends MyCanvasV3 {
	MyPedal pedal = new MyPedal((WIDTH/2)-50,590);
	MyBall ball = new MyBall((WIDTH/2)-15,HEIGHT-40);
	public MyCanvasV4() {
		super();
	}
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g); 
		Graphics2D g2d = (Graphics2D)g; 
		g2d.setPaint(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		Color[] color= {Color.RED,Color.ORANGE,Color.YELLOW,Color.GREEN,Color.CYAN,Color.BLUE,Color.MAGENTA};
		int width=80;
		int height=20;
		for(int count=0;count<=9;count++) {
			for (int i=0;i<=6;i++) {
				g2d.setStroke(new BasicStroke(5));
				brick.x=0+count*width;
				brick.y=60+i*height;
				g2d.setColor(color[i]);
				g2d.fill(brick);
				g2d.setColor(Color.BLACK);
				g2d.draw(brick);
			}
		}
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

	}

}
