package saprom.saranyu.lab10;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4{

	public void addComponents() {
		super.addComponents();
		
		LabelName.setFont(new Font ("Serif",Font.PLAIN,14));
		LabelModel.setFont(new Font ("Serif",Font.PLAIN,14));
		LabelWeight.setFont(new Font ("Serif",Font.PLAIN,14));
		LabelPrice.setFont(new Font ("Serif",Font.PLAIN,14));
		LabelMobile.setFont(new Font ("Serif",Font.PLAIN,14));
		TxtName.setFont(new Font("Serif",Font.PLAIN,14));
		TxtModel.setFont(new Font("Serif",Font.PLAIN,14));
		TxtWeight.setFont(new Font("Serif",Font.PLAIN,14));
		TxtPrice.setFont(new Font("Serif",Font.PLAIN,14));
		TypeLabel.setFont(new Font("Serif",Font.PLAIN,14));
		ReviewLabel.setFont(new Font("Serif",Font.PLAIN,14));
		Box.setFont(new Font("Serif",Font.PLAIN,14));
		FeaturesLabel.setFont(new Font("Serif",Font.PLAIN,14));
		Featureslist.setFont(new Font("Serif",Font.PLAIN,14));
		ReviewLabel.setFont(new Font("Serif",Font.PLAIN,14));
		Scroll.setFont(new Font("Serif",Font.PLAIN,14));
		ButtonCancel.setForeground(Color.red);
		ButtonOK.setForeground(Color.blue);
	}
	
	public MobileDeviceFormV5(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 MobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");
		MobileDeviceFormV5.addComponents();
		MobileDeviceFormV5.addMenus();
		MobileDeviceFormV5.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
