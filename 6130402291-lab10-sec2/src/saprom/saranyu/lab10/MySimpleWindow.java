package saprom.saranyu.lab10;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	protected JPanel PanelButton;
	protected JPanel PanelWindow;
	protected JButton ButtonCancel;
	protected JButton ButtonOK;
	protected JPanel PanelLabel;
	protected JPanel PanelTxt;

	public MySimpleWindow(String title) {
		super(title);
	}
	protected void addComponents() {
	
		ButtonCancel = new JButton("Cancel");
		ButtonOK = new JButton("OK");
		
		PanelButton = new JPanel();
		PanelWindow = new JPanel();
		PanelButton.add(ButtonCancel,BorderLayout.WEST);
		PanelButton.add(ButtonOK,BorderLayout.EAST);
		
		PanelWindow=(JPanel)this.getContentPane();
		PanelWindow.setLayout(new BorderLayout());
		
		PanelWindow.add(PanelButton,BorderLayout.SOUTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	 protected void setFrameFeatures() {
		 this.setLocationRelativeTo(null);
		 this.setVisible(true);
		 this.pack();
	 }
	 public static void createAndShowGUI() {
		 MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		 msw.addComponents();
		 msw.setFrameFeatures();
	 }

	 public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	 }

}
