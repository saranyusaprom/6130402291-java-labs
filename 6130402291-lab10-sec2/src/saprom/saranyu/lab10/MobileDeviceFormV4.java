package saprom.saranyu.lab10;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {
	protected BufferedImage Image;
	protected JMenuItem Red;
	protected JMenuItem Green;
	protected JMenuItem Blue;
	protected JMenuItem Sixteen;
	protected JMenuItem twenty;
	protected JMenuItem twentyfour;
	

	protected void addComponents() {
		super.addComponents();
	
	}

	protected void addMenus() {
		super.addMenus();
	
		updateMenuIcon();
		addSubMenus();
	}

	private void addSubMenus() {
		Red = new JMenuItem("Red");
		Green = new JMenuItem("Green");
		Blue = new JMenuItem("Blue");
		Sixteen = new JMenuItem("16");
		twenty = new JMenuItem("20");
		twentyfour = new JMenuItem("24");
		
		ColorItem.add(Red);
		ColorItem.add(Green);
		ColorItem.add(Blue);
		SizeItem.add(Sixteen);
		SizeItem.add(twenty);
		SizeItem.add(twentyfour);
	}

	private void updateMenuIcon() {
		NewItem.setIcon(new ImageIcon("images/new.jpg"));

	}

	public MobileDeviceFormV4(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV4 MobileDeviceFormV4 = new MobileDeviceFormV4("Mobile Device Form V4");
		MobileDeviceFormV4.addComponents();
		MobileDeviceFormV4.addMenus();
		MobileDeviceFormV4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
