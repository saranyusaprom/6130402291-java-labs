package saprom.saranyu.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	protected JMenuItem CustomItem;

	public MobileDeviceFormV8(String title) {
		super(title);
	}
	public void addMnemonic() {
		FileMenu.setMnemonic(KeyEvent.VK_F);
		NewItem.setMnemonic(KeyEvent.VK_N);
		OpenItem.setMnemonic(KeyEvent.VK_O);
		SaveItem.setMnemonic(KeyEvent.VK_S);
		ExitItem.setMnemonic(KeyEvent.VK_X);
		ConfigMenu.setMnemonic(KeyEvent.VK_C);
		ColorItem.setMnemonic(KeyEvent.VK_L);
		Blue.setMnemonic(KeyEvent.VK_B);
		Green.setMnemonic(KeyEvent.VK_G);
		Red.setMnemonic(KeyEvent.VK_R);
		CustomItem.setMnemonic(KeyEvent.VK_U);
	}

	public void addAccelerator() {
		NewItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		OpenItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		SaveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		ExitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		Blue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		Green.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		Red.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		CustomItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
	}
	public void addComponents() {
		super.addComponents();
		CustomItem = new JMenuItem("Custom...");
	}
	public void addMenus() {
		super.addMenus();
		ColorItem.add(CustomItem);
		addMnemonic();
		addAccelerator();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceFormV8.addComponents();
		mobileDeviceFormV8.addMenus();
		mobileDeviceFormV8.setFrameFeatures();
		mobileDeviceFormV8.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
