package saprom.saranyu.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener{
	
	public MobileDeviceFormV7(String title) {
		super(title);
	}
	public void handleOKButton() {
		if(IOS.isSelected()) {
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + TxtName.getText() + ", Model Name: " + TxtModel.getText()
					+ ", Weight: " + TxtWeight.getText() + ", Price: " + TxtPrice.getText() + 
					"\n" + " OS: " + IOS.getText()+ "\n Type: " + Box.getSelectedItem()
					+ "\n Features: " + Featureslist.getSelectedValuesList() + "\n Review: " + ReviewArea.getText());
		}
		else if(Android.isSelected()){	
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + TxtName.getText() + ", Model Name: " + TxtModel.getText()
					+ ", Weight: " + TxtWeight.getText() + ", Price: " + TxtPrice.getText() + 
					"\n" + " OS: " + Android.getText() + "\n Type: " + Box.getSelectedItem()
					+ "\n Features: " + Featureslist.getSelectedValuesList() + "\n Review: " + ReviewArea.getText());
		}
	}
	public void handleCancelButton() {
		TxtName.setText(" ");
		TxtModel.setText(" ");
		TxtWeight.setText(" ");
		TxtPrice.setText(" ");
		ReviewArea.setText(" ");	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {

		Object src = e.getSource();
		if (src == ButtonOK) {
			handleOKButton();
			
		} else if (src == ButtonCancel) {
			handleCancelButton();
		}
	}
	

	protected void addListeners() {
		ButtonOK.addActionListener(this);
		ButtonCancel.addActionListener(this);
		
		IOS.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				if(IOS.isSelected()) {
					JOptionPane.showMessageDialog(null, "Your os platform is now changed to iOS");
				}
			}
		});
		
		Android.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
	
				if(Android.isSelected()) {
					JOptionPane.showMessageDialog(null, "Your os platform is now changed to Android OS");

				}
			}
		});
				
		Featureslist.addListSelectionListener(new ListSelectionListener(){
		      public void valueChanged(ListSelectionEvent event) {
		          if (event.getValueIsAdjusting())
		            return;
		          JOptionPane.showMessageDialog(null, Featureslist.getSelectedValuesList());
		        }
		      });
		
		Box.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (e.getStateChange() == ItemEvent.SELECTED) {
			          JOptionPane.showMessageDialog(null, "Type is updated to " + Box.getSelectedItem());
				}
			}
		});
	}
	public void addComponents() {
		super.addComponents();
		Featureslist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		Featureslist.setSelectedIndex(3);
		Featureslist.addSelectionInterval(0, 1);
		
		Android.setSelected(true);
	}
	
	public void addMenus() {
		super.addMenus();
	}

	

	public static void createAndShowGUI() {
		MobileDeviceFormV7 MobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		MobileDeviceFormV7.addComponents();
		MobileDeviceFormV7.addMenus();
		MobileDeviceFormV7.setFrameFeatures();
		MobileDeviceFormV7.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
