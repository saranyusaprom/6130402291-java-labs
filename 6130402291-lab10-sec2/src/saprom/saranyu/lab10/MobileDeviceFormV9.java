package saprom.saranyu.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
	
	public MobileDeviceFormV9(String title) {

		super(title);
	}
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		
		Object src = event.getSource();
		JFileChooser fileChooser = new JFileChooser ( );

		if (src == OpenItem) {
			fileChooser.setDialogTitle("Open");
			int openButton = fileChooser.showDialog (null, "Open" );
			
			if (openButton == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile ();
                String fileName = fileChooser.getName (selectedFile);
                JOptionPane.showMessageDialog (null, "Opening file " + fileName);
			}
			else {
                JOptionPane.showMessageDialog (null, "Open command cancelled by user.");
			}
			
		} else if(src == SaveItem) {
			fileChooser.setDialogTitle("Save");
			int saveButton = fileChooser.showDialog (null, "Save");
			
				if (saveButton == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					String fileName = fileChooser.getName(selectedFile);
					JOptionPane.showMessageDialog (null, "Saving file " + fileName);
				}
				else {
					JOptionPane.showMessageDialog (null, "Save command cancelled by user.");
				}
				
		} else if(src == CustomItem) {
			Color newColor = JColorChooser.showDialog(null, "Choose color", getBackground());
				if(newColor != null) {
					ReviewArea.setBackground(newColor);
				}
				
		} else if (src == Red) {
			ReviewArea.setBackground(Color.RED);
			
		} else if(src == Green) {
			ReviewArea.setBackground(Color.GREEN);
			
		}else if(src == Blue) {
			ReviewArea.setBackground(Color.BLUE);
			
		}else if(src == ExitItem) {
			System.exit(0);
		}
	}

	
	public void addListeners() {
		OpenItem.addActionListener(this);
		SaveItem.addActionListener(this);
		ExitItem.addActionListener(this);
		Red.addActionListener(this);
		Green.addActionListener(this);
		Blue.addActionListener(this);
		
		CustomItem.addActionListener(this);
	}
	
	public void addComponents() {

		super.addComponents();
	}
	
	public void addMenus() {

		super.addMenus();
	}
	public static void createAndShowGUI() {

		MobileDeviceFormV9 mobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
		mobileDeviceFormV9.addComponents();
		mobileDeviceFormV9.addMenus();
		mobileDeviceFormV9.setFrameFeatures();
		mobileDeviceFormV9.addListeners();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
