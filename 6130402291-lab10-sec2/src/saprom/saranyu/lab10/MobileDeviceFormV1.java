package saprom.saranyu.lab10;

import java.awt.*;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV1 extends MySimpleWindow {
	protected JRadioButton IOS;
	protected JRadioButton Android;
	protected JLabel LabelName;
	protected JLabel LabelModel;
	protected JLabel LabelWeight;
	protected JLabel LabelPrice;
	protected JLabel LabelMobile;
	protected JTextField TxtName;
	protected JTextField TxtModel;
	protected JTextField TxtWeight;
	protected JTextField TxtPrice;
	protected JPanel choicePanel;
	protected JPanel NorthPanel1;
	protected JPanel PanelName;
	protected JPanel PanelModel;
	protected JPanel PanelPrice;
	protected JPanel PanelWeight;
	protected JPanel PanelMobile;
	protected JPanel GNorth;



	

	public MobileDeviceFormV1(String title) {
		super(title);
	}
	
	protected void addComponents() {
		super.addComponents();

		GNorth = new JPanel();
		GNorth.setLayout(new GridLayout(5,1));
		NorthPanel1 = new JPanel();
		PanelName = new JPanel();
		PanelModel = new JPanel();
		PanelPrice = new JPanel();
		PanelWeight = new JPanel();
		PanelMobile = new JPanel();
		choicePanel=new JPanel();


		
		TxtName=new JTextField(15);
		TxtModel=new JTextField(15);
		TxtWeight=new JTextField(15);
		TxtPrice=new JTextField(15);

		LabelName = new JLabel("Brand Name:");
		LabelModel = new JLabel("Model Name:");
		LabelWeight = new JLabel("Weight(kg.):");
		LabelPrice = new JLabel("Price (Baht):");
		LabelMobile = new JLabel("Mobile OS:");
		
		IOS = new JRadioButton("iOS");
		Android = new JRadioButton("Android");

//		JRadioButton Android = new JRadioButton();
//		Android.setText("Android");
//		JRadioButton OS = new JRadioButton();
//		OS.setText("OS");
		ButtonGroup select=new ButtonGroup();
		select.add(Android);
		select.add(IOS);
		
		PanelName.setLayout(new BorderLayout());
		PanelModel.setLayout(new BorderLayout());
		PanelWeight.setLayout(new BorderLayout());
		PanelPrice.setLayout(new BorderLayout());
		PanelMobile.setLayout(new BorderLayout());

		PanelName.add(LabelName,BorderLayout.WEST);
		PanelModel.add(LabelModel,BorderLayout.WEST);
		PanelWeight.add(LabelWeight,BorderLayout.WEST);
		PanelPrice.add(LabelPrice,BorderLayout.WEST);
		PanelMobile.add(LabelMobile,BorderLayout.WEST);
		
		PanelName.add(TxtName,BorderLayout.EAST);
		PanelModel.add(TxtModel,BorderLayout.EAST);
		PanelWeight.add(TxtPrice,BorderLayout.EAST);
		PanelPrice.add(TxtWeight,BorderLayout.EAST);

		
		choicePanel.add(Android);
		choicePanel.add(IOS);
		
		PanelMobile.add(choicePanel,BorderLayout.EAST);
		
		GNorth.add(PanelName);
		GNorth.add(PanelModel);
		GNorth.add(PanelPrice);
		GNorth.add(PanelWeight);
		GNorth.add(PanelMobile);
		
		NorthPanel1.setLayout(new BorderLayout());
		NorthPanel1.add(GNorth,BorderLayout.NORTH);
		PanelWindow.add(NorthPanel1);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public static void createAndShowGUI() {
		 MobileDeviceFormV1 MobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		 MobileDeviceFormV1.addComponents();
		 MobileDeviceFormV1.setFrameFeatures();
	 }										
													
	 public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	 }
}
