package saprom.saranyu.lab5;

/**
 * 
 * This java program can set and get brand name, model name, price.
 * 
 * @author Saranyu Saprom
 * @version 1.0
 * @since 2019-02-24
 * 
 *
 */
public class AndroidSmartWatch extends AndroidDevice {
	private String modelName;
	private String brandName;
	private int price;

	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}

	/**
	 * 
	 * @param brandName is brand from your code main
	 * @param modelName is Model name from your code main
	 * @param price     is price from your code main
	 */

	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate, and your step count");
	}

	@Override
	public String toString() {
		return "AndroidSmartWatch[Brand name:" + getBrandName() + ", Model name: " + getModelName() + ", Price:"
				+ getPrice() + " Baht]";
	}
}
