package saprom.saranyu.lab5;

/**
 * This java program is get and set andriodversion, modelName, price of Samsung.
 * 
 * @author Saranyu Saprom
 * @version 1.0
 * @since 2019-02-24
 *
 */
public class SamsungDevice extends MobileDevice {
	private static String brand = "Samsung";
	private double andriodversion;

	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
	}

	public double getAndroidVersion() {
		return andriodversion;
	}

	public void setAndroidVersion(float version) {
		this.andriodversion = version;
	}

	public static String getBrand() {
		return brand;
	}

	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}

	public SamsungDevice(String modelName, int price, float andriodversion) {
		super(modelName, "Andriod", price);
		this.andriodversion = andriodversion;
	}

	public SamsungDevice(String modelName, int price, float weight, double andriodversion) {
		super(modelName, "Andriod", price, weight);
		this.andriodversion = andriodversion;
	}

	@Override
	public String toString() {
		return "SamsungDevice [Model Name:" + super.getModelName() + ", Os: " + super.getOs() + ", Price:"
				+ super.getPrice() + " Baht, Weight" + super.getWeight() + " g, Andriod version:" + getAndroidVersion()
				+ "]";
	}
}
