package saprom.saranyu.lab5;

/**
 * This java program is set and get modelName, os, price, weight of Mobile.
 * 
 * @author Saranyu Saprom
 * @version 1.0
 * @since 2019-02-24
 *
 */
public class MobileDevice {
	private String modelName;
	private String os;
	private int price;
	private float weight;

	/**
	 * 
	 * This is attributes of MobileDevice
	 * 
	 * @param modelName is Model name from your code main
	 * @param os        is Operation system such android, iOS and windowsPhone
	 * @param price     is price from your code main
	 * @param weight    is weight from your code main
	 */

	public MobileDevice(String modelName, String os, int price, float weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}

	public MobileDevice(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = 0;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getModelName() {
		return modelName;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getOs() {
		return os;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getPrice() {
		return price;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return "MobileDevice [Model name:" + getModelName() + ", Os:" + getOs() + ", Price:" + getPrice()
				+ "Baht, Weight:" + getWeight() + "g]";
	}

}
