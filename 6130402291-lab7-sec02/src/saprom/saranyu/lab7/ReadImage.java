package saprom.saranyu.lab7;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ReadImage extends JPanel{
	BufferedImage img;
	
	public void paint(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}
	
	protected ReadImage() {
		try {
			img = ImageIO.read(new File("images/galaxyNote9.jpg"));
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}
	
	public Dimension getPreferredSize() {
		if(img == null) {
			return new Dimension(100,100);
		}else {
			return new Dimension(img.getWidth(),img.getHeight());
		}
	}

}