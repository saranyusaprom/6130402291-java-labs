package saprom.saranyu.lab7;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV6 extends MobileDeviceFormV5{
	protected JPanel SouthPanel;
	public void addComponents() {
		
		super.addComponents();
		PanelWindow.remove(PanelButton);
		ReadImage image = new ReadImage();
		
		SouthPanel =new JPanel();
		SouthPanel.setLayout(new BorderLayout());
		SouthPanel.add(image,BorderLayout.NORTH);
		SouthPanel.add(PanelButton,BorderLayout.SOUTH);
		
		PanelWindow.add(SouthPanel,BorderLayout.SOUTH);
	}
	
	public MobileDeviceFormV6(String title) {
		super(title);
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV6 MobileDeviceFormV6 = new MobileDeviceFormV6("Mobile Device Form V6");
		MobileDeviceFormV6.addComponents();
		MobileDeviceFormV6.addMenus();
		MobileDeviceFormV6.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
